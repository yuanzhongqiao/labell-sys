<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><a href="https://github.com/heartexlabs/label-studio"><img alt="/readme/images/labelimg.png" src="https://github.com/HumanSignal/labelImg/raw/master/readme/images/labelimg.png" style="max-width: 100%;"></a>
<a name="user-content-label-studio-is-a-modern-multi-modal-data-annotation-tool"></a>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Label Studio 是一款现代化的多模式数据注释工具</font></font></h2><a id="user-content-label-studio-is-a-modern-multi-modal-data-annotation-tool" class="anchor" aria-label="永久链接：Label Studio 是一款现代化、多模式数据注释工具" href="#label-studio-is-a-modern-multi-modal-data-annotation-tool"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LabelImg 是一款流行的图像注释工具，由 Tzutalin 在数十名贡献者的帮助下创建，现已停止积极开发，并已成为 Label Studio 社区的一部分。查看</font></font><a href="https://github.com/heartexlabs/label-studio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Label Studio</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，它是用于图像、文本、超文本、音频、视频和时间序列数据的最灵活的开源数据标记工具。</font></font><a href="https://labelstud.io/guide/install.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Label Studio 并加入</font></font><a href="https://label-studio.slack.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">slack 社区</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即可开始使用。</font></font></p>
<a href="https://github.com/heartexlabs/label-studio"><img alt="/readme/images/label-studio-1-6-player-screenshot.png" src="https://github.com/HumanSignal/labelImg/raw/master/readme/images/label-studio-1-6-player-screenshot.png" style="max-width: 100%;"></a>
<a name="user-content-about-labelimg"></a>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于 LabelImg</font></font></h2><a id="user-content-about-labelimg" class="anchor" aria-label="固定链接：关于 LabelImg" href="#about-labelimg"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<a href="https://pypi.python.org/pypi/labelimg" rel="nofollow"><img src="https://camo.githubusercontent.com/fb7ae8e0392488311d8b355b5e36b7bd63918a598af6a529a6c67914447c15af/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f6c6162656c696d672e737667" data-canonical-src="https://img.shields.io/pypi/v/labelimg.svg" style="max-width: 100%;">
</a>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/6e56b631ec961fbb85c8ec25d5a2ff6dbc99b7f9a337598b33b8c0506df8415d/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f776f726b666c6f772f7374617475732f747a7574616c696e2f6c6162656c496d672f5061636b6167653f7374796c653d666f722d7468652d6261646765"><img alt="GitHub 工作流程状态" src="https://camo.githubusercontent.com/6e56b631ec961fbb85c8ec25d5a2ff6dbc99b7f9a337598b33b8c0506df8415d/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f776f726b666c6f772f7374617475732f747a7574616c696e2f6c6162656c496d672f5061636b6167653f7374796c653d666f722d7468652d6261646765" data-canonical-src="https://img.shields.io/github/workflow/status/tzutalin/labelImg/Package?style=for-the-badge" style="max-width: 100%;"></a></p>
<a href="https://github.com/tzutalin/labelImg"><img src="https://camo.githubusercontent.com/e749ce5e3e78158a3c7f6140f576719f4804504db5378cbde9a3646f49fba819/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c616e672d656e2d626c75652e737667" data-canonical-src="https://img.shields.io/badge/lang-en-blue.svg" style="max-width: 100%;">
</a>
<a href="https://github.com/tzutalin/labelImg/blob/master/readme/README.zh.rst"><img src="https://camo.githubusercontent.com/46c5a60f181de898bd4837f40ab5f1f821083a1984e26594b9d80432e75d52ad/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c616e672d7a682d677265656e2e737667" data-canonical-src="https://img.shields.io/badge/lang-zh-green.svg" style="max-width: 100%;">
</a>
<a href="https://github.com/tzutalin/labelImg/blob/master/readme/README.jp.rst"><img src="https://camo.githubusercontent.com/1c5b9fe2cbd025b9f11e03cd02d71decbd38be798b86e26644a1dc59f98ae6e0/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c616e672d6a702d677265656e2e737667" data-canonical-src="https://img.shields.io/badge/lang-jp-green.svg" style="max-width: 100%;">
</a>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LabelImg 是一个图形图像注释工具。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它是用 Python 编写的，并使用 Qt 作为其图形界面。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="http://www.image-net.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注释以ImageNet</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用的 PASCAL VOC 格式的 XML 文件形式保存</font><font style="vertical-align: inherit;">。此外，它还支持 YOLO 和 CreateML 格式。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/tzutalin/labelImg/master/demo/demo3.jpg"><img alt="演示图像" src="https://raw.githubusercontent.com/tzutalin/labelImg/master/demo/demo3.jpg" style="max-width: 100%;"></a></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/tzutalin/labelImg/master/demo/demo.jpg"><img alt="演示图像" src="https://raw.githubusercontent.com/tzutalin/labelImg/master/demo/demo.jpg" style="max-width: 100%;"></a></p>
<p dir="auto"><a href="https://youtu.be/p0nR2YsCY_U" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">观看演示视频</font></font></a></p>
<a name="user-content-installation"></a>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h3><a id="user-content-installation" class="anchor" aria-label="固定链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<a name="user-content-get-from-pypi-but-only-python3-0-or-above"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 PyPI 获取但仅限 python3.0 或更高版本</font></font></h4><a id="user-content-get-from-pypi-but-only-python30-or-above" class="anchor" aria-label="永久链接：从 PyPI 获取，但仅限 python3.0 或更高版本" href="#get-from-pypi-but-only-python30-or-above"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是现代 Linux 发行版（如 Ubuntu 和 Fedora）上最简单的（一条命令）安装方法。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pip3 install labelImg
labelImg
labelImg [IMAGE_PATH] [PRE-DEFINED CLASS FILE]</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip3 install labelImg
labelImg
labelImg [IMAGE_PATH] [PRE-DEFINED CLASS FILE]" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<a name="user-content-build-from-source"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建</font></font></h4><a id="user-content-build-from-source" class="anchor" aria-label="永久链接：从源代码构建" href="#build-from-source"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux/Ubuntu/Mac 至少需要</font></font><a href="https://www.python.org/getit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 2.6</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，并且已使用</font></font><a href="https://www.riverbankcomputing.com/software/pyqt/intro" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyQt 4.8</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进行了测试。然而，</font><font style="vertical-align: inherit;">强烈建议使用</font></font><a href="https://www.python.org/getit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 3 或更高版本</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和  </font></font><a href="https://pypi.org/project/PyQt5/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyQt5 。</font></font></a><font style="vertical-align: inherit;"></font></p>
<a name="user-content-ubuntu-linux"></a>
<div class="markdown-heading" dir="auto"><h5 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ubuntu Linux</font></font></h5><a id="user-content-ubuntu-linux" class="anchor" aria-label="永久链接：Ubuntu Linux" href="#ubuntu-linux"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 3 + Qt5</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>sudo apt-get install pyqt5-dev-tools
sudo pip3 install -r requirements/requirements-linux-python3.txt
make qt5py3
python3 labelImg.py
python3 labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]</pre><div class="zeroclipboard-container">
     
  </div></div>
<a name="user-content-macos"></a>
<div class="markdown-heading" dir="auto"><h5 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苹果系统</font></font></h5><a id="user-content-macos" class="anchor" aria-label="固定链接：macOS" href="#macos"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 3 + Qt5</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>brew install qt  <span class="pl-c"><span class="pl-c">#</span> Install qt-5.x.x by Homebrew</span>
brew install libxml2

or using pip

pip3 install pyqt5 lxml <span class="pl-c"><span class="pl-c">#</span> Install qt and lxml by pip</span>

make qt5py3
python3 labelImg.py
python3 labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]</pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 3 虚拟环境 (推荐)</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Virtualenv 可以避免很多 QT / Python 版本问题</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>brew install python3
pip3 install pipenv
pipenv run pip install pyqt5==5.15.2 lxml
pipenv run make qt5py3
pipenv run python3 labelImg.py
[Optional] rm -rf build dist<span class="pl-k">;</span> pipenv run python setup.py py2app -A<span class="pl-k">;</span>mv <span class="pl-s"><span class="pl-pds">"</span>dist/labelImg.app<span class="pl-pds">"</span></span> /Applications</pre><div class="zeroclipboard-container">
     
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：最后一个命令会在您的 /Applications 文件夹中为您提供一个漂亮的 .app 文件，其中包含一个新的 SVG 图标。您可以考虑使用脚本：build-tools/build-for-macos.sh</font></font></p>
<a name="user-content-windows"></a>
<div class="markdown-heading" dir="auto"><h5 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视窗</font></font></h5><a id="user-content-windows" class="anchor" aria-label="固定链接：Windows" href="#windows"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font><a href="https://www.python.org/downloads/windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、
 </font></font><a href="https://www.riverbankcomputing.com/software/pyqt/download5" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyQt5</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
并</font></font><a href="http://lxml.de/installation.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 lxml</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打开cmd，进入</font></font><a href="#labelimg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">labelImg</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pyrcc4 -o libs/resources.py resources.qrc
For pyqt5, pyrcc5 -o libs/resources.py resources.qrc

python labelImg.py
python labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]</pre><div class="zeroclipboard-container">
  
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果你想打包成一个单独的EXE文件</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>Install pyinstaller and execute:

pip install pyinstaller
pyinstaller --hidden-import=pyqt5 --hidden-import=lxml -F -n <span class="pl-s"><span class="pl-pds">"</span>labelImg<span class="pl-pds">"</span></span> -c labelImg.py -p ./libs -p ./</pre><div class="zeroclipboard-container">
   
  </div></div>
<a name="user-content-windows-anaconda"></a>
<div class="markdown-heading" dir="auto"><h5 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows + Anaconda</font></font></h5><a id="user-content-windows--anaconda" class="anchor" aria-label="永久链接：Windows + Anaconda" href="#windows--anaconda"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载并安装</font></font><a href="https://www.anaconda.com/download/#download" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Anaconda</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（Python 3+）</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打开 Anaconda Prompt 并转到</font></font><a href="#labelimg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">labelImg</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>conda install pyqt=5
conda install -c anaconda lxml
pyrcc5 -o libs/resources.py resources.qrc
python labelImg.py
python labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]</pre><div class="zeroclipboard-container">
    
  </div></div>
<a name="user-content-use-docker"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Docker</font></font></h4><a id="user-content-use-docker" class="anchor" aria-label="永久链接：使用 Docker" href="#use-docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker run -it \
--user <span class="pl-s"><span class="pl-pds">$(</span>id -u<span class="pl-pds">)</span></span> \
-e DISPLAY=unix<span class="pl-smi">$DISPLAY</span> \
--workdir=<span class="pl-s"><span class="pl-pds">$(</span>pwd<span class="pl-pds">)</span></span> \
--volume=<span class="pl-s"><span class="pl-pds">"</span>/home/<span class="pl-smi">$USER</span>:/home/<span class="pl-smi">$USER</span><span class="pl-pds">"</span></span> \
--volume=<span class="pl-s"><span class="pl-pds">"</span>/etc/group:/etc/group:ro<span class="pl-pds">"</span></span> \
--volume=<span class="pl-s"><span class="pl-pds">"</span>/etc/passwd:/etc/passwd:ro<span class="pl-pds">"</span></span> \
--volume=<span class="pl-s"><span class="pl-pds">"</span>/etc/shadow:/etc/shadow:ro<span class="pl-pds">"</span></span> \
--volume=<span class="pl-s"><span class="pl-pds">"</span>/etc/sudoers.d:/etc/sudoers.d:ro<span class="pl-pds">"</span></span> \
-v /tmp/.X11-unix:/tmp/.X11-unix \
tzutalin/py2qt4

make qt4py2<span class="pl-k">;</span>./labelImg.py</pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以拉取已安装并需要所有依赖项的镜像。</font></font><a href="https://youtu.be/nw1GexJzbCI" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">观看演示视频</font></font></a></p>
<a name="user-content-usage"></a>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用法</font></font></h3><a id="user-content-usage" class="anchor" aria-label="固定链接：用法" href="#usage"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<a name="user-content-steps-pascalvoc"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤（PascalVOC）</font></font></h4><a id="user-content-steps-pascalvoc" class="anchor" aria-label="永久链接：步骤 (PascalVOC)" href="#steps-pascalvoc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用上述说明进行构建和启动。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单击菜单/文件中的“更改默认保存的注释文件夹”</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">点击‘打开目录’</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">点击“创建 RectBox”</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单击并释放鼠标左键选择一个区域来注释矩形框</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以使用鼠标右键拖动矩形框来复制或移动它</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注释将保存到您指定的文件夹中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以参考以下热键来加快您的工作流程。</font></font></p>
<a name="user-content-steps-yolo"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤（YOLO）</font></font></h4><a id="user-content-steps-yolo" class="anchor" aria-label="永久链接：步骤（YOLO）" href="#steps-yolo"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定义</font></font><code>data/predefined_classes.txt</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将用于训练的类别列表。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用上述说明进行构建和启动。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在工具栏“保存”按钮正下方，点击“PascalVOC”按钮，即可切换到YOLO格式。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以使用 Open/OpenDIR 来处理单张或多张图像。处理完一张图像后，单击“保存”。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO 格式的 txt 文件将以相同的名称保存在与图像相同的文件夹中。名为“classes.txt”的文件也保存在该文件夹中。“classes.txt”定义 YOLO 标签引用的类名列表。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">笔记：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在处理图像列表的过程中，标签列表不得发生变化。保存图像时，classes.txt 也会更新，但之前的注释不会更新。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保存为 YOLO 格式时，不应使用“默认类”功能，否则将不会被引用。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当保存为 YOLO 格式时，“困难”标志将被丢弃。</font></font></li>
</ul>
<a name="user-content-create-pre-defined-classes"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建预定义类</font></font></h4><a id="user-content-create-pre-defined-classes" class="anchor" aria-label="永久链接：创建预定义类" href="#create-pre-defined-classes"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以编辑
</font></font><a href="https://github.com/tzutalin/labelImg/blob/master/data/predefined_classes.txt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">data/predefined_classes.txt</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
来加载预定义的类</font></font></p>
<a name="user-content-annotation-visualization"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注释可视化</font></font></h4><a id="user-content-annotation-visualization" class="anchor" aria-label="永久链接：注释可视化" href="#annotation-visualization"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将现有的标签文件复制到与图像相同的文件夹中。标签文件名必须与图像文件名相同。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单击文件并选择“打开目录”，然后打开图像文件夹。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在文件列表中选择图像，它将出现该图像中所有对象的边界框和标签。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（在视图中选择显示标签模式来显示/隐藏标签）</font></font></p>
<a name="user-content-hotkeys"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">热键</font></font></h4><a id="user-content-hotkeys" class="anchor" aria-label="固定链接：热键" href="#hotkeys"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>




<tbody valign="top">
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl + u</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从目录中加载所有图像</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl + r</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更改默认注释目标目录</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl + s</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">节省</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl + d</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复制当前标签和矩形框</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl + Shift + d</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">删除当前图片</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">空间</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将当前图像标记为已验证</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瓦</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建一个矩形框</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">d</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下一张图片</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">A</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上一张图片</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德尔</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">删除选定的矩形框</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl++</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">放大</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ctrl--</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缩小</font></font></td>
</tr>
<tr><td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">↑→↓←</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">键盘箭头移动选定的矩形框</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">验证图像：</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按下空格键时，用户可以将图像标记为已验证，背景将变成绿色。这在自动创建数据集时使用，然后用户可以浏览所有图片并对其进行标记，而不是对其进行注释。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">难的：</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">困难字段设置为 1 表示该对象已被注释为“困难”，例如，一个清晰可见但在没有大量使用上下文的情况下难以识别的对象。根据您的深度神经网络实现，您可以在训练期间包括或排除困难对象。</font></font></p>
<a name="user-content-how-to-reset-the-settings"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何重置设置</font></font></h4><a id="user-content-how-to-reset-the-settings" class="anchor" aria-label="永久链接：如何重置设置" href="#how-to-reset-the-settings"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果在加载类时出现问题，您可以：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 labelimg 的顶部菜单中单击菜单/文件/全部重置</font></font></li>
<li><dl>
<dt><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从主目录中删除 .labelImgSettings.pkl。在 Linux 和 Mac 中，您可以执行以下操作：</font></font></dt>
<dd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">rm ~/.labelImgSettings.pkl</font></font></dd>
</dl>
</li>
</ol>
<a name="user-content-how-to-contribute"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何贡献</font></font></h4><a id="user-content-how-to-contribute" class="anchor" aria-label="永久链接：如何贡献" href="#how-to-contribute"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发送拉取请求</font></font></p>
<a name="user-content-license"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h4><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://github.com/tzutalin/labelImg/blob/master/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">免费软件：MIT 许可证</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引用：Tzutalin。LabelImg。Git 代码（2015 年）。https: </font></font><a href="https://github.com/tzutalin/labelImg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//github.com/tzutalin/labelImg</font></font></a></p>
<a name="user-content-related-and-additional-tools"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相关工具和其他工具</font></font></h4><a id="user-content-related-and-additional-tools" class="anchor" aria-label="永久链接：相关及其他工具" href="#related-and-additional-tools"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ol dir="auto">
<li><a href="https://github.com/heartexlabs/label-studio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Label Studio</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为机器学习和人工智能标记图像、文本、音频、视频和时间序列数据</font></font></li>
<li><a href="https://github.com/tzutalin/ImageNet_Utils"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ImageNet Utils</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于下载图像、创建机器学习的标签文本等</font></font></li>
<li><a href="https://hub.docker.com/r/tzutalin/py2qt4" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Docker运行labelImg</font></font></a></li>
<li><a href="https://github.com/tensorflow/models/blob/4f32535fe7040bb1e429ad0e3c948a492a89482d/research/object_detection/g3doc/preparing_inputs.md#generating-the-pascal-voc-tfrecord-files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成 PASCAL VOC TFRecord 文件</font></font></a></li>
<li><a href="https://www.elegantthemes.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于 Nick Roach 的图标 (GPL) 的应用程序图标</font></font></a></li>
<li><a href="https://tzutalin.blogspot.com/2019/04/set-up-visual-studio-code-for-python-in.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 vscode 中设置 Python 开发</font></font></a></li>
<li><a href="https://code.ihub.org.cn/projects/260/repository/labelImg" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本项目在iHub平台的链接</font></font></a></li>
<li><a href="https://github.com/tzutalin/labelImg/tree/master/tools"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将注释文件转换为 CSV 格式或适用于 Google Cloud AutoML 的格式</font></font></a></li>
</ol>
<a name="user-content-stargazers-over-time"></a>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随着时间的推移，观星者</font></font></h4><a id="user-content-stargazers-over-time" class="anchor" aria-label="永久链接：随时间推移的观星者" href="#stargazers-over-time"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/b6e03b9c2a9492def12836bf9aa3b70e3e84edd06d536b6aa590a2de705b7227/68747470733a2f2f7374617263686172742e63632f747a7574616c696e2f6c6162656c496d672e737667"><img src="https://camo.githubusercontent.com/b6e03b9c2a9492def12836bf9aa3b70e3e84edd06d536b6aa590a2de705b7227/68747470733a2f2f7374617263686172742e63632f747a7574616c696e2f6c6162656c496d672e737667" data-canonical-src="https://starchart.cc/tzutalin/labelImg.svg" style="max-width: 100%;"></a></p>

</article></div>
